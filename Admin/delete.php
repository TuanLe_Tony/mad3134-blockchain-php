<?php
  include 'header_css.php';
?>
<?php
  include 'menu.php';
?>
<!--main code-->
<?php
  include 'database.php';

	if ($_SERVER["REQUEST_METHOD"] == "GET") {
		$id = $_GET["id"];
		$query = "SELECT * from access WHERE ID =". $id;

		$results = mysqli_query($con, $query);

		if ($results == FALSE) {
			echo mysqli_error($con);
			die();
		}

		$access = mysqli_fetch_assoc($results);
	}
	else if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$id = $_POST["access_id"];

    $query = 	"DELETE FROM access ";
    $query .= 	"WHERE ID=" . $id;

		$results = mysqli_query($con, $query);

		if ($results) {
			echo "OKAY! <br>";

			header("Location: Access.php?email='.<?php echo $userEmail;?>'");

		}
		else {
			echo "BAD! <br>";
			echo mysqli_error($con);
		}

	}

?>
<!DOCTYPE html5>
<html>
<head>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
	<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
	<style type="text/css">
		.mdl-grid {
			max-width:1024px;
			margin-top:40px;
		}

		h1 {
			font-size:36px;
		}
		h2 {
			font-size:30px;
		}
	</style>

</head>
<body>
  <div class="box">
  	<div class="mdl-grid" style="text-align:center;">
  	  <div class="mdl-cell mdl-cell--12-col">
  	  	<h1> ADMIN - DELETE </h1>
  		<h3> Access Hash: <?php echo $access["Access_hash"] ?> </h3>
  		<!-- form -->
  		<form action="delete.php" method="POST">
  			<input name="access_id"value="<?php echo $id; ?>" hidden=true>
  		  <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
  			 Confirm
  		  </button>
  		</form>
  		<br>
  		<a href="Access.php" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
  			Cancel
  		</a>
  	  </div>
  	</div>
  </div>

</body>
</html>
<!--end-->

<?php
  include 'rooter.php';
?>
