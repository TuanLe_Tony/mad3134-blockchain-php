<?php
  include 'header_css.php';
?>
<?php
  include 'menu.php';
?>
<?php
  include 'database.php';
if ($_SERVER["REQUEST_METHOD"] == "GET") {

  $id = $_GET["id"];
  // 3. Query
  $query = "SELECT * FROM access WHERE ID = " . $id;

  $results = mysqli_query($con, $query);
  $access = mysqli_fetch_assoc($results);

}
else if ($_SERVER["REQUEST_METHOD"] == "POST") {

  $id = $_POST["access_id"];
  $update = $_POST["access_hash"];
  //md5 hash
  $MDHash = md5($update);
  //SHA-256 hash
  $SHA256HASH = bin2hex(mhash(MHASH_SHA256,$MDHash));
  //md5 hash second time
  $MDHash1 = md5($SHA256HASH);

  $query = "UPDATE access SET Access_hash='$MDHash1' WHERE ID='$id'";

  echo $query ."<br>";

  $results = mysqli_query($con, $query);

  if ($results) {
    echo "OKAY! <br>";

    header("Location: Access.php");

  }
  else {
    echo "BAD! <br>";
    echo mysqli_error($con);
  }


}
 ?>
 <!DOCTYPE html5>
 <html>
 <head>
 	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
 	<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
 	<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
 	<style type="text/css">
 		.mdl-grid {
 			max-width:1024px;
 			margin-top:40px;
 		}

 		h1 {
 			font-size:36px;
 		}
 		h2 {
 			font-size:30px;
 		}
 	</style>

 </head>
 <body>
<div class="box">
 	<div class="mdl-grid" style='text-align:center;'>
 	  <div class="mdl-cell mdl-cell--12-col">
 	  	<h1> ADMIN - UPDATE </h1>
 		<!-- form -->

 		<form action="update.php" method="POST">
 			<input name="access_id" value="<?php echo $id; ?>" hidden>


 			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">

 				<input 	name="access_hash" class="mdl-textfield__input" type="text" id="sample3">


 				<label class="mdl-textfield__label" for="sample3" style="text-align:center;">UPDATE NEW ACCESS - HASH</label>
 			</div>
      <br>
 		  <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
 			 Confirm
 		  </button>
 		</form>
 		<br>
	   <a href="Access.php" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
			Cancel
		</a>
 	  </div>
 	</div>
</div>
 </body>
 </html>
 <!-- endcode  -->
 <?php
   include 'rooter.php';
 ?>
