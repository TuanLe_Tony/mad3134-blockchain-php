<?php
  include 'header_css.php';
?>
<?php
  include 'menu.php';
?>

<!-- My main code -->
<?php
  include_once 'database.php';

  if (isset($_POST['btninsert'])) {

    $insert = $_POST['insert'];
    //md5 hash
    $MDHash = md5($insert);
    //SHA-256 hash
    $SHA256HASH = bin2hex(mhash(MHASH_SHA256,$MDHash));
    //md5 hash second time
    $MDHash1 = md5($SHA256HASH);

    $sql= "SELECT * FROM Access WHERE Access_hash = '$MDHash1' ";
    $query =
      'INSERT INTO Access (Access_hash) ' .
      'VALUES ("' . $MDHash1 . '")';
    $results = mysqli_query($con, $query);
    echo "<h3 class='subtitle is-3' style='text-align:center; color:green;' > Insert succeed </h3>";
  }
  else if (isset($_POST['btndelete'])) {
    echo "delete";
  }

?>
<h2 class="subtitle is-2" style="text-align:center;"> ADMIN ACCESS - SETTING </h2>

<form class="" action="Access.php" method="POST">

  <div class="columns is-mobile is-multiline is-centered" style="">
     <div class="column is-half">
       <code>
          <div class="field">
            <div class="control">
              <input class="input is-success" type="text" placeholder="Insert" name="insert" >
              <input class="button is-success" type="submit" value="Insert" name="btninsert" >
              <input class='input is-success' type='text' placeholder='Update' name='update' hidden>
            </div>
          </div>
        </code>
      </div>
    </div>

</form>

<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--12-col">

  <h2 class="subtitle is-2" style="text-align:center;"> ACCESS LIST </h2>
  <!-- access table -->
  <!-- @TODO: Insert PHP code here -->
  <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp" >
    <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric" >Acess Hash</th>
    </tr>
    </thead>
    <tbody>
<?php
include 'database.php';
//@TODO: You need to jumble PHP and HTML code here.
$query = "SELECT * from access";

// 4. SEND QUERY TO DB & GET RESULTS
$results = mysqli_query($con, $query);
// loop through the database results
while( $access = mysqli_fetch_assoc($results) ) {
  echo "<tr>";
    echo "<td class='mdl-data-table__cell--non-numeric'>";
      echo $access["Access_hash"];
    echo "</td>";
    echo "<td>";
      echo "<a href='update.php?id=" . $access["ID"] . "'>";
        echo "<i class='far fa-edit'></i>";
      echo "</a>";
    echo "</td>";
    echo "<td>";
      echo "<a href='delete.php?id=" . $access["ID"] . "'>";
        echo "<i class='fas fa-trash-alt'></i>";
      echo "</a>";
    echo "</td>";
  echo "</tr>";
}
?>
<!-- END My main code -->
<?php
  include 'rooter.php';
  ?>
