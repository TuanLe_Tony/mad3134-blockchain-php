
# BLOCK CHAIN-APP

Sample code for a Block Chain web app. Written in HTML/CSS/JS + PHP + MySQL

## Software Requirements - Scenario 
### - Need to find the hash that starts with 4 digits (exp : 8888xxxxxxxx)
### - The Nonce of next block (Input) combine privious hash to generate a correct hash of it's own hash --> which means the nonce's number that you find will increase following the block 2,3 and 4

* XAMPP (or similar)
* Internet browser (Chrome)

# CLIENTS INTERACT.

# Register - page
![Scheme](ScreenshotsImages/register.jpg)

After registed you able to login

# Login - page
![Scheme](ScreenshotsImages/login.jpg)

After logined you able to see the account-detail
# Account-detail - page
![Scheme](ScreenshotsImages/afterlogin.jpg)

## Select "BlockChain" from menu.
# Blockchain - page
![Scheme](ScreenshotsImages/blockchain.jpg)
Example for 4 blockchains

![Scheme](ScreenshotsImages/example.jpg)

## Select "Get Started" from menu.
# Blockchain - Get Started - page
![Scheme](ScreenshotsImages/getstartblockchain.jpg)

Players need to find the right hash to access and reward
## Select "Access" from menu.
# Access - page
![Scheme](ScreenshotsImages/accesshash.jpg)

If the right hash you are able to see this.

# Access - reward - page
![Scheme](ScreenshotsImages/reward.jpg)

## Select "The Icon on left side (detail)" from menu.

# Account-detail - page (updated)
![Scheme](ScreenshotsImages/addcoin.jpg)


# ADMIN INTERACT.

# Login - page
![Scheme](ScreenshotsImages/adminLogin.jpg)


## Only admin@gmail.com ables to login in this.
ADMIN HOME has some things that admin can do here.
# Blockchain - Get Started - page
![Scheme](ScreenshotsImages/Mining.jpg)

## Admin ables to do the mine and get the correct hashes (The right hashes always show you 3 digits begin with 8888xxxxxxx ).

# Blockchain - Get Started - page
![Scheme](ScreenshotsImages/SetupAccess.jpg)

## Admin ables to insert, delete and update the access hash OR shut down all the access hashes.








