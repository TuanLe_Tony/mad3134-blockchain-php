<?php

  echo "<section class='section'>";


  echo "<div class='container'>";
    echo "<h1 class='title' style='text-align:center;'>";
      echo "LOGIN";
    echo "</h1>";

    echo "<form method='POST' action='Login.php'>";

      echo "<div class='field'>";
        echo "<label class='label'>Email</label>";
      echo  "<div class='control has-icons-left'>";
          echo "<input class='input' type='email' placeholder='hello@thanks.com' >";


        echo  "<span class='icon is-small is-left'>";
      echo "<i class='fas fa-envelope'></i>";
      echo    "</span>";

      echo  "</div>";
    echo  "</div>";

    echo  "<div class='field'>";
    echo    "<label class='label'>Password</label>";
    echo    "<div class='control has-icons-left'>";
      echo    "<input class='input' type='password' placeholder='Enter your Password' >";

        echo  "<!-- add a icon to the input box -->";
      echo    "<span class='icon is-small is-left'>";
      echo      "<i class='fas fa-unlock'></i>";
      echo    "</span>";
      echo  "</div>";
    echo  "</div>";

echo  "</section>";
?>
