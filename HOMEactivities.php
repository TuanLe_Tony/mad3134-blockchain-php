<?php
session_start();
?>
<!DOCTYPE>
<html>
  <head>
  </head>
  <style type="text/css">
  </style>
  <body>


    <!-- MENU -->
    <nav class="level">
      <p class="level-item has-text-centered">
        <a href="about.php" class="link is-info">About Us</a>
      </p>
      <p class="level-item has-text-centered">
        <a href="locations.php" class="link is-info">Locations</a>
      </p>
      <p class="level-item has-text-centered">
        <a href="index.php">
           <img src="images/logo-donut.png" class="logo">
         </a>
      </p>
      <p class="level-item has-text-centered">
        <a href="menu.php" class="link is-info">Menu</a>
      </p>
      <p class="level-item has-text-centered">
        <a href="cart.php" class="button is-danger is-outlined">
          <i class="fas fa-shopping-cart"></i>
          <span id="cartAmt"></span>
        </a>
      </p>
    </nav>

    <script type="text/javascript">

    </script>
  </body>
</html>
