<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Tuan awesome!</title>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css">
		<script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
	</head>
	<style type="text/css">
			p button {
				padding-left: 50px;
				padding-right: 50px;
				font-weight: bold;

			}
			p button:hover{
				background-color: black;
				color:white;
			}
			.field{
				margin-left: 200px;
				margin-right: 200px;
			}
	</style>
	<body>

	<section class="section">
		<div class="container">
			<h1 class="title" style='text-align:center;'>
				REGISTER
			</h1>

			<!-- form fields go here -->
	    <form method="POST" action="checkRegister.php">

	  		<div class="field">
	  			<label class="label">Email</label>
	  			<div class="control has-icons-left">
	  				<input class="input" type="email" placeholder="hello@thanks.com" name="email" >

	  				<!-- add a icon to the input box -->
	  				<span class="icon is-small is-left">
	  					<i class="fas fa-envelope"></i>
	  				</span>

	  			</div>
	  		</div>

	      <div class="field">
	        <label class="label">Password</label>
	        <div class="control has-icons-left">
	          <input class="input" type="password" placeholder="Enter your Password" name="password" pattern=".{0}|.{6,}"   required title="Either 0 OR (6 chars minimum)">

	          <!-- add a icon to the input box -->
	          <span class="icon is-small is-left">
	            <i class="fas fa-unlock"></i>
	          </span>

	        </div>
	      </div>
	      <div class="field">
	        <label class="label">Confirm Password</label>
	        <div class="control has-icons-left">
	          <input class="input" type="password" placeholder="Confirm Password" name="cfpassword" pattern=".{0}|.{6,}"   required title="Either 0 OR (6 chars minimum)">

	          <!-- add a icon to the input box -->
	          <span class="icon is-small is-left">
	            <i class="fas fa-unlock"></i>
	          </span>

	        </div>
		      <input class="input" type="password" placeholder="Confirm Password" name="coin" value="10" hidden='true'>
	      </div>

	  		<div class="field" style="padding-top:10px;">
	  			<p class="control" style='text-align:center;'>
	  				<button>Register</button>
	  			</p>
	  		</div>
	    </form>

			<!-- end of form -->
				<div style='text-align:center;' >
					<a href="Login.php" style='text-decoration:none;'><h4>I HAD AN ACCOUNT</h4></a>
				</div>
	    </div>
  </section>


	<link rel="stylesheet" href="https://unpkg.com/papercss@1.4.1/dist/paper.min.css">

  </body>
</html>
