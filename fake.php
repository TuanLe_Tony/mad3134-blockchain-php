<?php

  echo "<section class='section'>";

  echo "<div class='container'>";
    echo "<h1 class='title' style='text-align:center;'>";
      echo "REGISTER";
    echo "</h1>";

    echo "<form method='POST' action='Register.php'>";

      echo "<div class='field'>";
        echo "<label class='label'>Email</label>";
      echo  "<div class='control has-icons-left'>";
          echo "<input class='input' type='email' placeholder='hello@bulma.com' >";


        echo  "<span class='icon is-small is-left'>";
      echo "<i class='fas fa-envelope'></i>";
      echo    "</span>";

      echo  "</div>";
    echo  "</div>";

    echo  "<div class='field'>";
    echo    "<label class='label'>Password</label>";
    echo    "<div class='control has-icons-left'>";
      echo    "<input class='input' type='password' placeholder='Enter your Password' >";

        echo  "<!-- add a icon to the input box -->";
      echo    "<span class='icon is-small is-left'>";
      echo      "<i class='fas fa-unlock'></i>";
      echo    "</span>";

      echo  "</div>";
    echo  "</div>";
    echo  "<div class='field'>";
    echo    "<label class='label'>Confirm Password</label>";
      echo  "<div class='control has-icons-left'>";
      echo    "<input class='input' type='password' placeholder='Confirm Password' >";


        echo  "<span class='icon is-small is-left'>";
          echo  "<i class='fas fa-unlock'></i>";
        echo  "</span>";

      echo  "</div>";
    echo  "</div>";

echo  "</section>";
?>
