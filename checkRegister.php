<?php
session_start();
include_once 'database.php';
//get name of form
$coin = $_POST["coin"];
$email = $_POST["email"];
$password = $_POST["password"];
$cfpassword = $_POST["cfpassword"];
//using for check if email existed...
$sql= "SELECT * FROM Users WHERE Email = '$email' ";
$result = mysqli_query($con,$sql);
$check = mysqli_fetch_array($result);
//END of DATABASE METHOD HERE.
//check empty and deny --> Unnecessary because can use css for deny
if (empty($email) || empty($password) || empty($cfpassword)){
//alert
  echo "<br>";
  echo "<div class='row flex-spaces' style='text-align:center;' >";
    echo "<div class='alert alert-danger'><h3>Email or password is blank, please try again !! <span><a href='Register.php' style='text-decoration: none;'><h4> <i class='fas fa-times-circle'></i></h4></a></span> </h3></div>";
  echo "</div>";
//fake register
 include 'fake.php'; // from fake file.
}
//end
else if ($password != $cfpassword){
  //alert
    echo "<br>";
    echo "<div class='row flex-spaces' style='text-align:center;' >";
      echo "<div class='alert alert-danger'><h3>The password does not match, please try again !! <span><a href='Register.php' style='text-decoration: none;'><h4> <i class='fas fa-times-circle' ></i></h4></a></span> </h3></div>";
    echo "</div>";
  //fake register
  include 'fake.php'; // from fake file.
}
// if email exist in batabase will deny creating.
else if(isset($check)){
  //alert
    echo "<br>";
    echo "<div class='row flex-spaces' style='text-align:center;' >";
      echo "<div class='alert alert-danger'><h3>The email had been taken, please try again !! <span><a href='Register.php' style='text-decoration: none;'><h4> <i class='fas fa-times-circle' ></i></h4></a></span> </h3></div>";
    echo "</div>";


  //fake register

  include 'fake.php'; // from fake file.

}
else {
  //Hashing and Insert password
  $hashingmethod = password_hash($password, PASSWORD_DEFAULT); //hashing method
  // $hashingmethod = md5($password);

  $query =
    'INSERT INTO Users (Email, Password, Coin) ' .
    'VALUES ("' .$email . '","' . $hashingmethod . '","' . $coin . '")';

  // 3. get results
  $results = mysqli_query($con, $query);
  //alert
    echo "<br>";
    echo "<div class='row flex-spaces' style='text-align:center;' >";
      echo "<div class='alert alert-success'><h3> Register succeed with Email: . $email , Please select SIGN IN !!</h3>  </div>";
    echo "</div>";

    echo "<div style='text-align:center;'>";
      echo  "<a href='Login.php' style='text-decoration:none;'><h4>SIGN IN </h4></a>";
    echo "</div>";

}


 ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css">
		<script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>

    <link rel="stylesheet" href="https://unpkg.com/papercss@1.4.1/dist/paper.min.css">

    <style type="text/css">

			.field{
				margin-left: 200px;
				margin-right: 200px;
			}

	</head>
	<body>
  </body>
</html>
