<script type="text/javascript">
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn.onclick = function() {
    modal.style.display = "block";
}

</script>
<nav class="navbar" role="navigation" aria-label="dropdown navigation" style="float:right">
  <div class="navbar-item has-dropdown is-hoverable">
    <a class="navbar-link" style="color:blue;">
      <?php echo $userEmail;?>
    </a>

    <div class="navbar-dropdown">
      <a class="navbar-item" style="color:red;">
        Sign Out
      </a>
      <div class="navbar-item" style="color:black;">
        Nonsense Coin
      </div>
    </div>
  </div>
</nav>
</body>
</html>
