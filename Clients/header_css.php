<!DOCTYPE>
<html>
  <head>
    <link href="index.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="css/bulma.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  	<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
  	<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>

    <style type="text/css">
      nav {
        margin-top:40px;
      }
      img.logo {
        width:100px;
      }
      img.product-image {
        height:290px;
      }
      table{
        margin: 0px auto;
      }
      .start{
        text-align: center;
      }
    </style>
  </head>
  <body>
