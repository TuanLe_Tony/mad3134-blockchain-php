<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Tuan awesome!</title>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css">
		<script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
		<style type="text/css">
			p button {
				padding-left: 50px;
				padding-right: 50px;
				font-weight: bold;

			}
			p button:hover{
				background-color: black;
				color:white;
			}
			.field{
				margin-left: 200px;
				margin-right: 200px;
			}
			img{
				height: 600px;
				width: 400px;
			}
			#kanna{
				position: absolute;
		    top: 5%;
		    left: 70%;
		    margin-left:-50% of your image's width];
		    margin-top: -50% of your image's height];
			}
		</style>
	</head>
	<body>
	<section class="section">
		<div class="container">
			<h1 class="title" style='text-align:center;'>
				LOGIN
			</h1>

			<!-- form fields go here -->
	    <form method="POST" action="checkLogin.php">

	  		<div class="field">
	  			<label class="label">Email</label>
	  			<div class="control has-icons-left">
	  				<input class="input" type="email" placeholder="hello@thanks.com" name="Lemail" id="signinId" class="email-input">

	  				<!-- add a icon to the input box -->
	  				<span class="icon is-small is-left">
	  					<i class="fas fa-envelope"></i>
	  				</span>

	  			</div>
	  		</div>

	      <div class="field">
	        <label class="label">Password</label>
	        <div class="control has-icons-left">
						<input name="id" type="hidden">
	          <input class="input" type="password" placeholder="Enter your Password" name="Lpassword" pattern=".{0}|.{6,}"   required title="Either 0 OR (6 chars minimum)">
	          <span class="icon is-small is-left">
	            <i class="fas fa-unlock"></i>
	          </span>

	        </div>
	      </div>

	  		<div class="field" style="padding-top:10px;">
	  			<p class="control" style='text-align:center;'>
	  				<button>LOGIN</button>
	  			</p>
	  		</div>

	    </form>
			<!-- end of form -->
				<div style='text-align:center;' >
					<a href="Register.php" style='text-decoration:none;'><h4>REGISTER</h4></a>
				</div>

				<div class="field" >
						<span><input id="rememberChkBox" type="checkbox" name="remember"> <h5>remember me</h5></span>
	  		</div>

	    </div>


  </section>
	<!-- squery -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<!-- cute frame work -->
	<link rel="stylesheet" href="https://unpkg.com/papercss@1.4.1/dist/paper.min.css">
	<script type="text/javascript">

	//remeber me
		$(function () {
		    if (localStorage.chkbox && localStorage.chkbox != '') {
		        $('#rememberChkBox').attr('checked', 'checked');
		        $('#signinId').val(localStorage.username);

		    } else {
		        $('#rememberChkBox').removeAttr('checked');
		        $('#signinId').val('');

		    }

		    $('#rememberChkBox').click(function () {

		        if ($('#rememberChkBox').is(':checked')) {
		            // save username and password
		            localStorage.username = $('#signinId').val();
		            localStorage.chkbox = $('#rememberChkBox').val();
		        } else {
		            localStorage.username = '';
		            localStorage.chkbox = '';
		        }
		    });
		});


	</script>
  </body>
</html>
